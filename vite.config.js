import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'
import gltf from 'rollup-plugin-gltf';

// https://vitejs.dev/config/
export default defineConfig(async () => {
  const mdx = await import("@mdx-js/rollup")
  return {
    assetsInclude: ['**/*.gltf'],
    publicDir: 'static',
    base: '/web-portal/',
    resolve: {
      alias: [{ find: '@', replacement: path.resolve(__dirname, '/src') }],
    },
    optimizeDeps: {
      include: ["react/jsx-runtime"]
    },
    plugins: [
      react(),
      mdx.default({remarkPlugins: []}),
      gltf({
        include: 'assets/**/*.gltf',
        inline: false
      })
    ],
  }
})
