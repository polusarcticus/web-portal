const base = {
  borderRadius: '5px',
  padding: '8px',
  margin: '8px',
  linkFontWeight: '550'
}

export const darkTheme = {
  ...base,
  colors: {
    background: '#000000',
    text: 'rgba(127, 255, 212, 0.382)',
    title: 'rgba(127, 255, 212, 0.618)',
    danger: '#99101a',
    link: 'rgba(127, 255, 212, 1)',
    linkInverse:'#1414e0',
    success: '#569069',

  },
};

export const lightTheme = {
  ...base,
  colors: {
    background: '#62ADE8',
    text: '#0d0707',
    title: 'rgba(127, 255, 212, 0.618)',
    danger: '#99101a',
    link:'#1412d4',
    linkInverse: '#f8c104',
    success: '#569069',
  }
}

