import styled from "styled-components"
import {useWindowSize} from '@/hooks/useWindowSize'
const StyledTitle = styled.div`
    font-size: 28px;
    line-height:32px;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    margin: 0 auto;
    height: ${props => props.height}px;
`

export const Title =({name}) => {
    const { cellSize } = useWindowSize()
    return (<>
    <StyledTitle
        height={cellSize}
    >
        {name}
    </StyledTitle>
    </>)
}