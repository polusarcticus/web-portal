import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
export const ToneJsCategory = { name: 'ToneJs'}
import { mdiScriptText } from '@mdi/js';

export const HackathonCategory = {
  name: 'Hackathon',
}

export const DeSciCategory = {
  name: 'DeSci'
}

export const EdgeComputeCategory = {
  name: "Edge Compute"
}
export const PhysicsCategory = {
  name: 'Physics'
}
export const HistoryCategory = {
  name: 'History',
  icon: mdiScriptText
}

export const StarknetCategory = {
  name: 'Starknet'
}

export const FilecoinCategory = {
  name: 'Filecoin',
}
export const SubgraphCategory = {
  name: 'Subgraph',
}
export const MathCategory = {
  name: 'Math'
  
}
export const JavascriptCategory = {
  name: 'JS'
}
export const VideoCategory = {
  name: 'Video'
}
export const ReactCategory = {
    name: 'React'
}
export const VueCategory = {
    name: 'Vue'
}
export const AngularCategory = {
    name: 'Angular'
}

export const DjangoCategory = {
    name: 'Django'
}

export const SolidityCategory = {
    name: 'Solidity'
}

export const Web3jsCategory = {
    name: 'Web3js'
}
export const BlockchainCategory = {
  name: 'Blockchain'
}
export const EthereumCategory = {
  name: 'Ethereum'
}

export const EVMCategory = {
    name: 'EVM chains'
}
export const CosmosCategory = {
    name: 'cosmos'
}
export const GolangCategory = {
  name: 'Golang'
}
export const HLFCategory = {
    name: 'Hyperledger'
}

export const ThreeCategory = {
    name: 'Three.js'
}

export const IPFSCategory = {
    name: 'IPFS'
}

export const DockerCategory = {
  name: 'Docker'
}

export const StyledCategory = styled.div`
  display: inline;
  margin-right: 6px;
  line-height: 42px;
  overflow-wrap: break-word;
  padding: 6px 6px;
  border: 0.5px solid ${props => props.theme.colors.link};
  border-radius: 4px;
  opacity: 0.618;
  color: ${props => props.theme.colors.link};
  &:hover {
    opacity: 1;
  }
`
