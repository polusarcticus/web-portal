
import styled, {useTheme} from 'styled-components'

import {Icon} from '@mdi/react'
import { mdiWeatherSunny  } from '@mdi/js';
import { mdiWeatherNight  } from '@mdi/js';
import Switch from 'react-switch'

export const ThemeToggler = ({handleToggle, currentTheme}) => {
  const theme = useTheme()
  return (
    <div style={{position:'absolute', top:'4px', left:'4px', zIndex: 3}}>
      <Switch
        onColor={theme.colors.background}
        offColor={theme.colors.linkInverse}
        checkedIcon={<Icon path={mdiWeatherNight} />}
        uncheckedIcon={<Icon path={mdiWeatherSunny} />}
        onChange={handleToggle}
        checked={currentTheme === 'dark' ? true:false}/>
    </div>
  )
}
