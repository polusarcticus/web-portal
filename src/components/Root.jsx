import { useState, useEffect } from 'react'
import styled from 'styled-components'
import { useWindowSize } from '@/hooks/useWindowSize'

const StyledRoot = styled.div`
  display: grid;
  width: ${props => props.cellSize*props.cols}px;
  height: ${props => props.cellSize*props.rows}px;
  grid-template-rows: repeat(${props => props.rows}, ${props => props.cellSize}px);
  grid-template-columns: repeat(${props => props.cols}, ${props => props.cellSize}px);
  margin: 0 auto;
`

export const Root = ({children }) => {
  const {rows,cols,cellSize} = useWindowSize()
  return (<StyledRoot cellSize={cellSize} rows={rows}cols={cols}>{children}</StyledRoot>)
}
