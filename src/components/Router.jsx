import {useWindowSize} from '@/hooks/useWindowSize'
import styled from 'styled-components'
import VortexBlack from '@/assets/4-vortex-colours.gif'
import VortexWhite from '@/assets/4-colours-vortex-white.gif'
import VortexStill from '@/assets/4-vortex-still.jpg'
const StyledImage = styled.img`
  background-image: url(${VortexStill});
  background-size: contain;
  width: 100%;
  height: 100%;
    transform: scaleX(1);
  &:hover {
    transform: scaleX(-1);
    background-image: url(${VortexWhite});

  }
`

export const Router = ({prioritize}) => {
  const {isPortrait} = useWindowSize()

  return (
    <StyledImage
      onClick={() => {
      prioritize()
      }}
      style={{
        display: 'grid',
        gridRow: isPortrait ? 16 : 10,
        gridColumn: isPortrait ? 10: 16
      }}
    ></StyledImage>)
}
