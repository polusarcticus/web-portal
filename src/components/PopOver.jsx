import styled from "styled-components"
import {Icon} from '@mdi/react';
import { mdiCloseCircle  } from '@mdi/js';
import {useWindowSize} from '@/hooks/useWindowSize'

const StyledClose = styled(Icon)`
  position: absolute;
  left: ${props => props.theme.padding};
  top: ${props => props.theme.padding};
  text-align: right;
  color: ${props => props.theme.colors.danger};
  &:hover {
    color: ${props => props.theme.colors.linkInverse};
  }
`

export const CloseButton = ({handleClose}) => {
  return (
    <StyledClose
      size={1}
      path={mdiCloseCircle}
      onClick={handleClose}
    />
  )
}

export const StyledFocus = styled.div`
  position: absolute;
  width: 100vw;
  height:100vh;
  opacity: 0.618;
  background-color: black;
  z-index: 1;
  top: 0px;
  left: 0px;
`
export const StyledPopOver = styled.div`
  background-color: ${(props) => props.theme.colors.background};
  border: 1px solid ${props => props.theme.colors.text};
  position: absolute;
  margin-left: auto;
  margin-right: auto;
  width: ${props => props.isPortrait ? '85.4vw': '61.8vw'};
  top: 5vh;
  right: 0px;
  left: 0px;
  padding:  5vh 5vw 5vh 5vw;
  z-index: 2;
  overflow-y: scroll;
`

export const PopOver = ({handleTogglePopover, children}) => {
  const { isPortrait } = useWindowSize()
  return (
    <StyledPopOver isPortrait={isPortrait}>{children}</StyledPopOver>
  )
}
