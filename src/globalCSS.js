import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  ::-webkit-scrollbar {
    background: ${props => props.theme.colors.background}
  }
  ::-webkit-scrollbar-thumb {
    background-color: ${props=> props.theme.colors.text};
    border: 4px solid transparent;
    border-radius: 18px;
    background-clip: content-box;
  }
  body {
    margin: 0;
    padding: 0;
  }
  a {
    font-weight: ${props => props.theme.linkFontWeight};
    color: ${props => props.theme.colors.link};
    text-decoration: inherit;
  }
  a:hover {
    color: ${props => props.theme.colors.linkInverse};
  }

  :root {
    font-family: Consolas, Inter, Avenir, Helvetica, Arial, sans-serif;
    font-size: 18px;
    line-height: 23px;

    color-scheme: light dark;
    color: ${props => props.theme.colors.text};
    background-color: ${props => props.theme.colors.background};

    font-synthesis: none;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -webkit-text-size-adjust: 100%;
  }

  h1 {
    line-height: 33px;
  }

    `


