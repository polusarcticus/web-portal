import ReactDOM from 'react-dom/client'
import ReactGA from 'react-ga4';
ReactGA.initialize([{trackingId: 'G-LF75P1EM3Y'}])
ReactGA.send({hitType: 'pageview', page: window.location.pathname + window.location.search, title: window.document.title});
import {
    RouterProvider,
  } from "react-router-dom";
  import { router } from './router'
ReactDOM.createRoot(document.getElementById('root')).render(
    <RouterProvider router={router} />
)
