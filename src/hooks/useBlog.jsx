
import { useState, useCallback, useEffect } from 'react'

export const useBlog =  () => {
    const [files, setFiles] = useState([])
    const [loadingFiles, setLoadingFiles] =useState(true)
    const [maxPost, setMaxPost] = useState(0)
    const fetchFiles = useCallback( async () => {
        setLoadingFiles(true)
        const tempFiles = []
        let iterator = 1
        while (true) {
            console.log('finding files')
            try {
                const {default: file} = await import(/* @vite-ignore */`../blog/posts/${iterator}/${iterator}.mdx`)
                const mdx = file()
                const post = {
                    id: mdx.props.children[0].props.id,
                    date: new Date(mdx.props.children[0].props.date).toLocaleDateString('en-US'),
                    title: mdx.props.children[0].props.title,
                    categories: mdx.props.children[0].props.categories,
                    content: mdx
                }
                tempFiles.unshift(post)
                iterator++
            } catch (e) {
                console.log(e)
                console.log('hopefully no more files')
                setMaxPost(iterator -1)
                setLoadingFiles(false)
                break
            }
        }
        setFiles(tempFiles)
    }, [])

    useEffect(() => {
        if (files.length == 0) {
            fetchFiles()
        }
    }, [])

    return { files, loadingFiles } 

}


export const useRecentPosts = () => {
   
}
