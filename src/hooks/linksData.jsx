import {FaTelegram, FaGitlab, FaDiscord} from 'react-icons/fa'
import {BsMailbox2, BsTwitter} from 'react-icons/bs'
import {GoMarkGithub} from 'react-icons/go'
export const linksData = [
    {name: 'Github', logo: <GoMarkGithub size='1.618em' />, url: 'https://github.com/polus-arcticus'},
    {name: 'Gitlab', logo: <FaGitlab size='1.618em'/>, url: 'https://gitlab.com/polusarcticus'},
    {name: 'Twitter', logo: <BsTwitter size='1.618em'/>, url: 'https://twitter.com/polus_arcticus' },
    {name: 'Telegram', logo: <FaTelegram size='1.618em'/>, url: 'https://t.me/polusarcticus'},
    {name: 'Discord', logo: <FaDiscord size='1.618em'/>, url: 'https://discordapp.com/users/polus_arcticus#6848'},
    {name: 'Email', logo: <BsMailbox2 size='1.618em'/>, url: 'mailto:thulsmans.1133@gmail.com' },
]