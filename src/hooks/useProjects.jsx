import {useState,useEffect,useCallback} from 'react'
import * as Categories from '@/components/Categories'
import { projectsData } from './projectsData.js'


export const useProjects = (filter={filterBy:null, filterFor:null}, sort = 'yearby') => {
    const [currentProjectsData, setCurrentProjectsData] = useState(projectsData)
    const [currentSort, setCurrentSort] = useState([sort])
    const [currentFilter, setCurrentFilter]  = useState(filter)
    
    const handleFilter = useCallback((filter=currentFilter) => {
        let filtered;
        switch (filter.filterBy) {
            case 'categories':
                filtered = currentProjectsData.filter((project) => {
                    return project.categories.find((category) =>{
                        return category.name == filter.filterFor
                    })
                })
                console.log('filtered,', filtered)
                break;
            case null:
                filtered = projectsData
                break
            default:
                filtered = projectsData.filter((project) => {
                    return project[filter.filterBy] == filter.filterFor
                })
            
        }
            setCurrentProjectsData(filtered)
            setCurrentFilter(filter)
    }, [])
    
    const handleSort = useCallback((sort=currentSort) => {
        console.log('handlesort')
        let sorted
        switch (sort) {
            case 'yearby':
                sorted = currentProjectsData.sort((a,b) => {
                    return  a.year > b.year
                })
                return
            default:
                sorted = currentProjectsData
        }
        setCurrentSort(sort)
        setCurrentProjectsData(sorted)

    }, [currentProjectsData])
    
    const handleResetFilters = useCallback(() => {
        setCurrentFilters([{filterBy:null, filterFor:null}])
        setCurrentProjectsData(projectsData)
    }, [])


    useEffect(() => {
        console.log('filtering', currentFilter)
        handleFilter()
        console.log('sorting', currentSort)
        handleSort()
    }, [])

    return {
        currentProjectsData:currentProjectsData,
         handleSort:handleSort,
          handleFilter:handleFilter, currentSort:currentSort, currentFilter:currentFilter}
}