import { useState, useCallback, useEffect } from "react";

export function fibonacci(num) {
  if (num <= 1) return 1;
  return fibonacci(num - 1) + fibonacci(num - 2);
}


export const useWindowSize = () => {
  const level = 7
  const [width, setWidth] = useState(window.innerWidth)
  const [height, setHeight] = useState(window.innerHeight)
  const [isPortrait, setIsPortrait] = useState(width >= height ? false:true)
  const [boxes, setBoxes] = useState(level)
  const [short, setShort] = useState(fibonacci(level - 1))
  const [long, setLong] = useState(fibonacci(level)) 
  const [rows, setRows] = useState(isPortrait ? long :short)
  const [cols, setCols] = useState(isPortrait ? short :long)
  const xmax = width / cols
  const ymax = height / rows
  let defaultCellSize;
  if (isPortrait) {
    defaultCellSize = (xmax >= ymax ? height: width) / (xmax >= ymax ? long: short)
  } else {
    defaultCellSize = (xmax >= ymax ? height: width) / (xmax >= ymax ? short: long)
  }
  const [cellSize, setCellSize] = useState(defaultCellSize)

  const handleResize = useCallback(() => {
    const newWidth = window.innerWidth
    const newHeight =window.innerHeight
    const newIsPortrait = (newWidth >= newHeight ? false :true)
    const newCols = newIsPortrait ? short :long
    const newRows = newIsPortrait ? long :short
    const xmax = newWidth / newCols
    const ymax = newHeight / newRows
    let defaultCellSize;
    if (newIsPortrait) {
      defaultCellSize = ((xmax >= ymax) ? newHeight: newWidth) / (xmax >= ymax ? long: short)
    } else {
      defaultCellSize = ((xmax >= ymax) ? newHeight: newWidth) / (xmax >= ymax ? short: long)
    }
    setCellSize(defaultCellSize)
    setRows(newRows)
    setCols(newCols)
    setWidth(newWidth)
    setHeight(newHeight)
    setIsPortrait((newWidth >= newHeight) ? false:true)
  }, [])

  useEffect(() => {
    window.addEventListener("resize", handleResize)
    handleResize()

    return () => window.removeEventListener("resize", handleResize)
  }, [])
  return {width, height, isPortrait, boxes, rows, cols, cellSize}
}
