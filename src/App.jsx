import { useState, StrictMode } from 'react'

import { ThemeProvider } from 'styled-components'
import { lightTheme, darkTheme } from './theme'
import GlobalCSS from './globalCSS'

import styled from 'styled-components'
import { ThemeToggler } from '@/components/ThemeToggler'
import { Root } from '@/components/Root'
import { Router } from '@/components/Router'
import { About } from '@/tiles/About/About'
import { Archive } from '@/tiles/Archive/Archive'
import { Projects } from '@/tiles/Projects/Projects'
import { Blog } from '@/tiles/Blog/Blog'
import { Links } from '@/tiles/Links/Links'
import { Finish } from '@/tiles/Finish/Finish'

import { useSearchParams } from "react-router-dom";

function App() {
  const [searchParams, setSearchParams]  = useSearchParams()
  console.log(searchParams.get('blogpostid')) 
  const [theme, setTheme] = useState('dark')
  const [i, setI] = useState(6)
  const prioritize = () => {
    setI((old) => {
      old -= 1
      if (old == 0) {
        old = 6
      }
      return old
    })
  }
  const handleToggle = () => {
    if (theme === 'light') setTheme('dark')
    if (theme === 'dark') setTheme('light')
  }

  return (
    <StrictMode>
      <ThemeProvider theme={() => {
        if (theme === 'light') {
        return lightTheme
        } else {
          return darkTheme
        }}}>
        <ThemeToggler handleToggle={handleToggle} currentTheme={theme} />
        <GlobalCSS />
        <Root>
          <About position={i % 6} />
          <Projects position={(i+1) % 6}/>
          <Blog position={(i+2) % 6}/>
          <Links position={(i+3) % 6}/>
          <Archive position={(i+4) % 6}/>
          <Finish position={(i+5) % 6}/>
          <Router prioritize={prioritize} />
        </Root>
      </ThemeProvider>
    </StrictMode>
  )
}

export default App
