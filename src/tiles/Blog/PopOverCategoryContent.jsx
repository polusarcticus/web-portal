import {useState,useEffect} from 'react'
import {StyledTitle } from './Styled'
import {StyledCategory} from '@/components/Categories'
import {StyledFocus, PopOver, CloseButton} from '@/components/PopOver'


export const PopOverCategoryContent = ({files, category, handleClose, handleCategoryClick, handlePostClick }) => {
  const [currentCategory, setCurrentCategory] = useState(category)
  const [content, setContent] = useState(null)

  useEffect(() => {
    setContent(
      files.filter((file,i) => {
        return file.categories.find((cat) => cat == currentCategory)
      }).map((relevant,j) => {
        const categories = relevant.categories.map((
          (cat, i) => {
            return (<StyledCategory key={i} onClick={() => {
              setCurrentCategory(cat)
            }}>{cat.name}</StyledCategory>)
          }
        ))
        return (
          <div key={j}>
            {relevant.id}: {relevant.date} -
            <StyledTitle onClick={() => {
              handlePostClick(relevant)
            }}>
              {relevant.title}
            </StyledTitle>
            <br/>
            {categories}
          </div>
        )
      })
    )
  }, [currentCategory])

  return (
    <>
    <PopOver>
      <CloseButton handleClose={handleClose} />
      <h1>Posts Containing: {currentCategory.name}</h1>
      {content && content}
    </PopOver>
      <StyledFocus onClick={handleClose} />
    </>
  )
}
