import { useEffect, useState } from 'react'
import { MDXProvider } from '@mdx-js/react'
import styled from 'styled-components'
import { Tile } from '@/tiles/Tiles'
import { useBlog } from '@/hooks/useBlog';
import { PopOverContent } from './PopOverContent'
import { PopOverCategoryContent } from './PopOverCategoryContent'
import { StyledTitle } from './Styled'
import { StyledCategory } from '@/components/Categories'
import { Title } from '@/components/Title'
import { useWindowSize } from '@/hooks/useWindowSize'

import { useSearchParams } from "react-router-dom";

const StyledBlogItem = styled.div`
  margin-left: ${props => props.theme.padding};
  margin-bottom: ${props => props.theme.padding};
`
export const Blog = ({ position }) => {
  const [showCategories, setShowCategories] = useState(false)
  const [category, setCategory] = useState('')

  const [showPost, setShowPost] = useState(false)
  const [post, setPost] = useState(null)
  const { cellSize } = useWindowSize()
  const { files, loadingFiles } = useBlog()

  const [searchParams, setSearchParams] = useSearchParams()
  const blogPost = searchParams.get('blogpostid')

  const components = {
    em: props => <i {...props} />
  }

  const handleClose = () => {
    setShowPost(false)
    setShowCategories(false)
  }

  const handleCategorySelect = () => {
  }

  const handleCategoryClick = (category) => {
    setShowPost(false)
    setCategory(category)
    setShowCategories(true)
  }
  const handlePostClick = (post) => {
    setShowCategories(false)
    setPost(post)
    setShowPost(true)
  }

  useEffect(() => {
    if (blogPost && !loadingFiles) {
      console.log(blogPost)
      console.log('file', files.find((file) => file.id == blogPost))
      handlePostClick(files.find((file) => file.id == blogPost))

    }
  }, [blogPost, loadingFiles])

  return (
    <Tile overflow="hidden scroll" position={position}>
        <Title name="Blog" />
        <MDXProvider components={components}>
          {files.length > 0 && (files.map((file, i) => {
            const categories = file.categories.map((
              (cat, i) => {
                return (<StyledCategory key={i} onClick={() => {
                  setCategory(cat)
                  setShowCategories(true)

                }}>{cat.name}</StyledCategory>)
              }
            ))
            return (
              <StyledBlogItem key={i}
              >
                <div style={{
                }}>{file.id}: {file.date} - <StyledTitle onClick={() => {
                  setPost(file)
                  setShowPost(true)
                }}>{file.title}</StyledTitle>
                </div>
                {categories}
              </StyledBlogItem>)
          }))
          }
        </MDXProvider>
      {showPost && (<PopOverContent file={post} handleClose={handleClose} handleCategoryClick={handleCategoryClick} />)}
      {showCategories && (<PopOverCategoryContent category={category} files={files} handleClose={handleClose} handleCategoryClick={handleCategoryClick} handlePostClick={handlePostClick} />)}


    </Tile>)

}
