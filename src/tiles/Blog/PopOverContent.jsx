import {MDXProvider} from '@mdx-js/react'
import {StyledCategory} from '@/components/Categories'
import {PopOver, StyledFocus, CloseButton} from '@/components/PopOver'

export const PopOverContent = ({file, handleClose, handleCategoryClick }) => {
  const categories = file.categories.map((
    (cat, i) => {
      return (<StyledCategory key={i} onClick={() => {
        handleCategoryClick(cat)
      }}>{cat.name}</StyledCategory>)
    }
  ))

  const components = {
    em: props => <i {...props} />
  }
  return (
    <>
    <PopOver>
      <CloseButton handleClose={handleClose} />
      <MDXProvider style={{lineHeight: '100px'}} components={components}>
        {categories}
        {file.content.props.children}
      </MDXProvider>

    </PopOver>
      <StyledFocus onClick={handleClose} />
    </>
  )
}
