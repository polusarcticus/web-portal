import styled from 'styled-components'
import {ReactNode} from 'react'
export const StyledTitle = styled.span`
  color: ${props => props.theme.colors.link};
  &:hover {
    color: ${props => props.theme.colors.linkInverse};
  }
`
