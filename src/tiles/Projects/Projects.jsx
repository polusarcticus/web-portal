import { Tile } from "../Tiles";
import * as Categories from '@/components/Categories'
import { Title } from '@/components/Title'
import { useWindowSize } from '@/hooks/useWindowSize'
import { useProjects } from '@/hooks/useProjects'
import styled from "styled-components";

const StyledProjectList = styled.div`
  display: block;
`
const StyledProjectTitle = styled.a`
  padding-bottom: ${props => props.theme.padding};
  margin-top: ${props => props.theme.padding};
`
const StyledDescription = styled.p`
  padding-top: ${props => props.theme.padding};
  padding-bottom: ${props => props.theme.padding};
  margin: 0;
`
const StyledRoleChit = styled(Categories.StyledCategory)`
  display: span;
  color: rgba(255,255,255,0.618);
  &:hover {
    color: rgba(255,255,255,1);
  }
`
const StyledChitBar = styled.div`
  margin-left: ${props => props.cellSize}px;
  margin-top: ${props => props.theme.padding};
`
const StyledYearChit = styled(Categories.StyledCategory)`
  background-color: ${props => props.theme.success};
  display: span;
  color: ${props => props.theme.colors.text};

`
const StyledDashline = styled.div`
margin: 0px ${props => props.cellSize}px ${props => props.theme.padding};
border-bottom: 2px solid ${props => props.theme.success}
`
export const ProjectListItem = ({project, handleYearFilter, handleRoleFilter, handleCategoryFilter }) => {
  const chits = project.categories.map((category, k) => {
    return (
      <Categories.StyledCategory key={k}
        onClick={() => {
          handleCategoryFilter(category.name)
        }}>{category.name}</Categories.StyledCategory>)
  })
  return (
    <div style={{marginTop: '12px', marginLeft: '12px'}}>
      <div>
        <StyledProjectTitle href={project.link} target="_blank">
          {project.name}</StyledProjectTitle>
      </div>
      <StyledChitBar >
        <StyledYearChit onClick={() => {handleYearFilter(project.year)}}>{project.year}</StyledYearChit>
        <StyledRoleChit onClick={() => {handleRoleFilter(project.role)}} >{project.role}</StyledRoleChit>
        {chits}
      </StyledChitBar>
      <StyledDescription>
        {project.description}
      </StyledDescription>
    </div>

  )
}


const ProjectsList = () => {
  const {currentProjectsData, handleFilter, currentFilter} = useProjects()
  
  const handleYearFilter = (year) => {
    handleFilter({filterBy: 'year', filterFor: year})
  }
  const handleRoleFilter = (role) => {
    handleFilter({filterBy: 'role', filterFor: role})
  }
  const handleCategoryFilter = (category) => {
    handleFilter({filterBy: 'categories', filterFor: category})
  }
  const handleClearFilters = () => {
    handleFilter({filterBy: null, filterFor: null})
  }

  let projects;
  
  if (currentProjectsData) {
    projects = currentProjectsData.map((project, i) => {
      return (
        <ProjectListItem
          key={i}
          project={project}
          handleYearFilter={handleYearFilter}
          handleRoleFilter={handleRoleFilter}
          handleCategoryFilter={handleCategoryFilter}></ProjectListItem>
      )
    })
  } else {
    projects  = (<span>Loading</span>)
  }
  return (<StyledProjectList>
    {currentFilter.filterFor && (<>Active Filter: <Categories.StyledCategory onClick={handleClearFilters}>{currentFilter.filterFor}</Categories.StyledCategory></>)}
    {projects}
  </StyledProjectList>
  )
}

export const Projects = ({ position }) => {

  return (
    <Tile overflow="hidden scroll" position={position}>
      <Title name="Projects" />
      <ProjectsList />
    </Tile>
  )
}
