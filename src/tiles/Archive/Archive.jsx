
import { Tile } from '@/tiles/Tiles'
import { Title } from '@/components/Title'
import { useWindowSize } from '@/hooks/useWindowSize'

export const Archive = ({position}) => {
  return (
    <Tile overflow="hidden scroll" position={position} >
      <Title name="Archive" />
      <h1 style={{textAlign:'center'}}>🚧</h1>
    </Tile>
  )
}
