import { useState } from 'react'
import styled from 'styled-components'
import {useWindowSize} from '@/hooks/useWindowSize'


const rowLevelMap= (position) => {
  switch (position) {
    case 0:
      return '1/-1'
    case 1:
      return '1/9'
    case 2:
      return '9/14'
    case 3:
      return '11/14';
    case 4:
      return '9/11';
    case 5:
      return '9/10';
  }
}
const colLevelMap= (position) => {
  switch (position) {
    case 0:
      return '1/14'
    case 1:
      return '14/-1'
    case 2:
      return '17/-1'
    case 3:
      return '14/17';
    case 4:
      return '14/16';
    case 5:
      return '16/17';
  }
}


const StyledTile = styled.div`
    border: ${props => props.theme.colors.text} solid 1px;
    overflow: ${props => props.overflow};
    grid-row: ${(props) => props.isPortrait ? colLevelMap(props.position) : rowLevelMap(props.position)};
    grid-column: ${(props) => props.isPortrait ? rowLevelMap(props.position) : colLevelMap(props.position)};
`
export const Tile = ({overflow, position, children}) => {
  const {isPortrait} = useWindowSize()
  return (
    <StyledTile
      isPortrait={isPortrait}
      position={position}
      overflow={overflow}
    >
      {children}
      </StyledTile>
  )
}
