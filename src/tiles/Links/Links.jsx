import styled from "styled-components"
import { Tile } from "@/tiles/Tiles"
import { Title} from '@/components/Title'
import { linksData } from "@/hooks/linksData"
import { useWindowSize } from '@/hooks/useWindowSize'

export const StyledLink = styled.a`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-left: 12px;
  margin-bottom: 6px;
  margin-right: 3px;
  line-height: 42px;
  overflow-wrap: break-word;
  padding: 6px 6px;
  border: 0.5px solid ${props => props.theme.colors.link};
  border-radius: 4px;
  opacity: 0.618;
  color: ${props => props.theme.colors.link};
  &:hover {
    opacity: 1;
  }
`
const Link = ({name, logo, url}) => {
  return <StyledLink target="_blank" href={url}>
    {logo}
    {name}</StyledLink>
}


export const Links = ({position}) => {
  const Chits = linksData.map((item, i) => {
    return (<Link key={i} name={item.name} logo={item.logo} url={item.url} />)
  })
  return (
    <Tile style={{display: 'grid'}} position={position} overflow="hidden scroll">
        <Title name="Links" />
        {Chits}
    </Tile>
  )
}
