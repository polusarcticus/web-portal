import { Tile } from "@/tiles/Tiles"

export const Finish = ({position}) => {
  return (<Tile overflow="hidden" position={position}>
    <div style={{
      display: 'flex',
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center',
      height: '100%',
      width: '100%'
    }}>
      <h1>🏁</h1>
    </div>
  </Tile>)
}
