import { GLTFLoader  } from 'three/examples/jsm/loaders/GLTFLoader'
import { useIntersect } from '@react-three/drei'
import { Canvas, useFrame, useThree, useLoader  } from '@react-three/fiber'
import React, { useRef, useState  } from 'react'
import * as THREE from 'three'
import cuboctahedronModel from '@/assets/about/cuboctahedron.gltf'
export function Cuboctahedron(props) {
  const { nodes, materials  } = useLoader(GLTFLoader, cuboctahedronModel)
  const visible = useRef(null)

  useFrame((state, delta) => {
    ref.current.rotation.x += 0.005
    ref.current.rotation.y += 0.005 
    ref.current.rotation.z += 0.01
})

  const ref = useIntersect((isVisible) => (visible.current = isVisible))
  return (
    <mesh
      ref={ref}
      position={props.position}
      scale={[0.618,0.618,0.618]}
      material={materials['Material.002']}
      geometry={nodes.Cube.geometry}>
      {props.children}
    </mesh>
  )
}
