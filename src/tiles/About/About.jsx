import { Suspense, useState, useEffect  } from 'react'
import { useWindowSize } from '@/hooks/useWindowSize'
import { Canvas  } from '@react-three/fiber'
import * as THREE from 'three'
import {
  useScroll,
  Scroll,
  ScrollControls,

} from '@react-three/drei'

import { Tile } from '@/tiles/Tiles'

import { Cuboctahedron } from '@/tiles/About/Cuboctahedron'
import { TitleText, TagText } from '@/tiles/About/Text'
import { MainAboutCanvas } from '@/tiles/About/MainAboutCanvas'
import {ImageItem} from '@/tiles/About/Image'

export const About = ({position}) => {
  const {isPortrait} = useWindowSize()
  const [activeComponent, setActiveComponent] = useState(<></>)

  useEffect(() => {
    switch (position) {
      case 0:
        setActiveComponent(<MainAboutCanvas />)
        break
    }
  }, [position])
  return (<Tile position={position}>{activeComponent}</Tile>)

}
