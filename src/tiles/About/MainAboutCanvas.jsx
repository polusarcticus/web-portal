import { Suspense, useState, useEffect  } from 'react'
import { useWindowSize } from '@/hooks/useWindowSize'
import { Canvas  } from '@react-three/fiber'
import * as THREE from 'three'
import {
  useScroll,
  Scroll,
  ScrollControls,

} from '@react-three/drei'
import { Cuboctahedron } from '@/tiles/About/Cuboctahedron'
import { TitleText, TagText } from '@/tiles/About/Text'
import { Title } from '@/components/Title'
import {ImageItem} from '@/tiles/About/Image'

import Moose from '@/assets/about/moose.jpg'
import Tardigrade from '@/assets/about/tardigrade-ipfs.png'
import Unicorn from '@/assets/about/unicorn-caducus.jpeg'
const textOne = "Inspired by Limewire and Bittorent, I choose in 2015 in the final stages of my economics degree to roll the dice on ethereum development as a career, with my first professional work beginning in 2018 building on Hyperledger Fabric. Solidity development began a year after with my professional work henceforth in prediction markets, automated market makers, decentralised social media"
const textTwo = "I found blockchain ecosystems to be the best place for an experimental economist, and enjoy writing prototypes of novel financial instruements that show whats possible in the blockchain model rather than tradfi model."
const textThree = "I enjoy programming web3 applications and bike packing around the globe. Pass me a message if you'd like to get in touch."
const heightSwitch = (isPortrait, cellSize) => {
  console.log('cellSize', cellSize)
  if (isPortrait) {
    if (cellSize > 60) { return 1.6 } else 
    if (cellSize > 50) { return 1.8 } else 
    if (cellSize > 40) { return 2.0 } else
    if (cellSize > 30) { return 2.5 } else
    if (cellSize > 20) { return 3.8 } else
    if (cellSize > 10) { return 6.1} else
    return 4.8

  } else {
    if (cellSize > 60) { return 1.6 } else 
    if (cellSize > 50) { return 1.7 } else 
    if (cellSize > 40) { return 1.8 } else 
    if (cellSize > 30) { return 2.5 } else 
    if (cellSize > 20) { return 4.9 } else 
    if (cellSize > 10) { return 6.0} else
    return 6.5

  }
}
export const MainAboutCanvas = ({}) => {
  const {isPortrait, cellSize} = useWindowSize()
  const [pages, setPages] = useState(heightSwitch(isPortrait, cellSize))
  useEffect(() => {
    setPages(heightSwitch(isPortrait,cellSize))
  }, [isPortrait,cellSize])
  return (
    <Canvas>
      <ScrollControls
        pages={pages} // Each page takes 100% of the height of the canvas
        distance={1} // A factor that increases scroll bar travel (default: 1)
        damping={0.2} // Friction, higher is faster (default: 4)
        horizontal={false} // Can also scroll horizontally (default: false)
        infinite={false} // Can also scroll infinitely (default: false)
      >
        <Scroll>
          <ambientLight />
          <TitleText />
          <TagText />
          <Cuboctahedron position={[-2.618,2.618,0]} />
        </Scroll>
        <Scroll html>
          <div style={{
            marginTop: '61.8%',
            marginLeft: 'auto',
            marginRight: 'auto',
            textAlign: 'center'
            }}>
              <Title name="About Me" />
            </div>
          <div style={{
            display: 'grid',
            gridTemplateColumns: '0.382fr 0.118fr 0.118fr 0.382fr',
            gridTemplateRows: '1fr 1fr 1fr',
            justifyItems: 'center',
            alignItems: 'center'
            }}>
            <p style={{
              gridColumn: '1/4',
              gridRow:1,
              margin: '5%'
            }}>{textOne}</p>
            <div style={{
              gridRow: 1,
              gridColumn: '4',
              textAlign: 'center'
              }}><img style={{objectFit:'contain', maxWidth:'76.4%'}}src={Moose}/></div>

            <p style={{
              gridColumn: '2/-1',
              gridRow:2,
              margin: '5%'
            }}>{textTwo}</p>
            <div style={{
              gridRow: 2,
              gridColumn: '1',
              textAlign: 'center'
              }}><img style={{objectFit:'contain', maxWidth:'76.4%'}}src={Tardigrade}/></div>
            <p style={{
              gridColumn: '1/4',
              gridRow:3,
              margin: '5%'
            }}>{textThree}</p>
            <div style={{
              gridRow: 3,
              gridColumn: '4',
              textAlign: 'center'
              }}><img style={{objectFit:'contain', maxWidth:'76.4%'}}src={Unicorn}/></div>
          </div>
        </Scroll>
      </ScrollControls>
    </Canvas>
  )
}
