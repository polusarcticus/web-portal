import { animated, useSpring   } from '@react-spring/three'
import Kickle from '@/assets/fonts/Kickle Cubicle.ttf'
import { Canvas, useFrame, useThree,  } from '@react-three/fiber'
import { Text } from '@react-three/drei'
import {useTheme} from 'styled-components'

export function TitleText() {
  const theme = useTheme()
  const { size  } = useThree()
  let scaleFactor 
  if (size.width <= 500 ) {
    scaleFactor = 0.236*(size.width / 400)
  } else {
    scaleFactor = 0.236*(size.width / 700)
  }
  const spring = useSpring({
    from: { scale: [0,-0.382,0]  },
    to: {
      scale: [scaleFactor,scaleFactor,scaleFactor],
      color: theme.colors.text
    },
    config: {
      friction: 10,

    },
    delay: 618,

  })
  const AnimatedText = animated(Text)
  return (
    <AnimatedText
      font={Kickle}
      {...spring}

      position={[0,0.618,0]}
    >
      Polus Arcticus
    </AnimatedText>
  )

}


export function TagText() {
  const theme = useTheme()
  const { size  } = useThree()
  let scaleFactor 
  if (size.width <= 500 ) {
    scaleFactor = 0.236*(size.width / 700)
  } else {
    scaleFactor = 0.236*(size.width / 1200)

  }
  const spring = useSpring({
    from: { scale: [0,0,0]  },
    to: {
      scale: [scaleFactor,scaleFactor,scaleFactor],
      color: theme.colors.text
    },
    config: {
      friction: 10,

    },
    delay: 1618,

  })
  const AnimatedText = animated(Text)
  return (
    <AnimatedText
      font={Kickle}
      {...spring}
      position={[0,-0.145,0]}
    >
      FullStack Ethereum Developer
    </AnimatedText>
  )

}
